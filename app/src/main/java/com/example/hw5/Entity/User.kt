package com.example.hw5.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class User (
    @PrimaryKey
    @ColumnInfo(name = "Name")
    val name: String
)