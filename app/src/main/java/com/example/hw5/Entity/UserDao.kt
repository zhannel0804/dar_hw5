package com.example.hw5.Entity

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {
    @Query("SELECT * FROM user ORDER BY name ASC")
    fun getAllUsers() : LiveData<List<User>>

    @Insert
    suspend fun insert(user: User)

    @Query("DELETE FROM user")
    fun deleteAll()
}