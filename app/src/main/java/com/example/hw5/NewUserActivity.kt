package com.example.hw5

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText


class NewUserActivity : AppCompatActivity() {

    private lateinit var editUserView: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)
        editUserView = findViewById(androidx.recyclerview.R.id.edit_user)

        val button = findViewById<Button>(androidx.recyclerview.R.id.button_save)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(editUserView.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val word = editUserView.text.toString()
                replyIntent.putExtra(EXTRA_REPLY, word)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }
    companion object {
        const val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
    }
}
